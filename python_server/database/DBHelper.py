import python_server.database.databaseConstants
import psycopg2
import time

databaseConstants = python_server.database.databaseConstants
# Class to help the user of the PostGres Database in the raspberryPiSms Project

class DBHelper:
    def __init__(self, host=None, port=None, database=None, user=None, password=None):
        if host is None or port is None or database is None or user is None or password is None:
            self.initUndefined()
        else:
            self.initDefined(host, port, database, user, password)
                
    def initDefined(self, host, port, database, user, password):
        try:
            self.connection = psycopg2.connect(user=user,
                                               password=password,
                                               host=host,
                                               port=port,
                                               database=database)
            self.cursor = self.connection.cursor()
        except (Exception, psycopg2.Error):
            self.initUndefined()
        
    def initUndefined(self):
        try:
            self.connection = psycopg2.connect(user=databaseConstants.USER,
                                               password=databaseConstants.PASSWORD,
                                               host=databaseConstants.HOST,
                                               port=databaseConstants.PORT,
                                               database=databaseConstants.DATABASE)
            self.cursor = self.connection.cursor()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        self.cursor.close()
        self.connection.close()
    
    # Create and delete tables #
    
    def deleteAll(self):
        self.cursor = self.connection.cursor()
        for i in {databaseConstants.MESSAGE_FRAGMENT_TABLE,
                  databaseConstants.USER_CONVERSATION_TABLE,
                  databaseConstants.USER_TABLE,
                  databaseConstants.CONVERSATION_TABLE}:
            self.cursor.execute('DROP TABLE IF EXISTS ' + i + ' CASCADE;')
        self.connection.commit()
        
    def createAll(self):
        self.cursor = self.connection.cursor()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS ' + databaseConstants.USER_TABLE + ' ('
                            '' + databaseConstants.USER_ID + ' SERIAL PRIMARY KEY ,'
                            '' + databaseConstants.USER_PHONE + ' VARCHAR (13) NOT NULL,'
                            '' + databaseConstants.USER_RSA + ' TEXT NOT NULL,'
                            '' + databaseConstants.USER_NAME + ' VARCHAR (40) );')
        self.connection.commit()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS ' + databaseConstants.CONVERSATION_TABLE + ' ('
                            '' + databaseConstants.CONVERSATION_ID + ' SERIAL PRIMARY KEY );')
        self.connection.commit()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ' ('
                            '' + databaseConstants.MESSAGE_ID + ' INTEGER NOT NULL,'
                            '' + databaseConstants.FRAGMENT_ID + ' INTEGER NOT NULL,'
                            '' + databaseConstants.USER_ID + ' INTEGER NOT NULL REFERENCES ' + databaseConstants.USER_TABLE + '(' + databaseConstants.USER_ID + ')' + ' ON DELETE CASCADE ,'
                            '' + databaseConstants.FRAGMENT_CONTENT + ' TEXT ,'
                            '' + databaseConstants.FRAGMENT_IS_FINAL + ' BOOLEAN NOT NULL DEFAULT FALSE,'
                            '' + databaseConstants.FRAGMENT_TIMESTAMP + ' BIGINT NOT NULL,'
                            ' PRIMARY KEY (' + databaseConstants.MESSAGE_ID + ' ,' + databaseConstants.FRAGMENT_ID + ' ,' + databaseConstants.USER_ID + ' ));')
        self.connection.commit()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS ' + databaseConstants.USER_CONVERSATION_TABLE + ' ('
                            '' + databaseConstants.USER_ID + ' INTEGER REFERENCES ' + databaseConstants.USER_TABLE + '(' + databaseConstants.USER_ID + ')' + ' ON DELETE CASCADE ,'
                            '' + databaseConstants.CONVERSATION_ID + ' INTEGER REFERENCES ' + databaseConstants.CONVERSATION_TABLE + '(' + databaseConstants.CONVERSATION_ID + ')' + ' ON DELETE CASCADE ,'
                            ' PRIMARY KEY (' + databaseConstants.USER_ID + ' ,' + databaseConstants.CONVERSATION_ID + ' ));')
        self.connection.commit()
        
    def clearAll(self):
        self.deleteAll()
        self.createAll()
    
    # Query on the user table #
    
    def addUser(self, phoneNumber, rsaPubKey, userName):
        self.cursor.execute('INSERT INTO ' + databaseConstants.USER_TABLE + ''
                            ' (' + databaseConstants.USER_PHONE + ',' + databaseConstants.USER_RSA + ',' + databaseConstants.USER_NAME + ')'
                            ' VALUES'
                            ' ( \'' + phoneNumber + '\',\'' + rsaPubKey + '\',\'' + userName + '\') '
                            ' RETURNING ' + databaseConstants.USER_ID + ';')
        self.connection.commit()
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
        
    def addUserWithoutName(self, phoneNumber, rsaPubKey):
        self.cursor.execute('INSERT INTO ' + databaseConstants.USER_TABLE + ''
                            ' (' + databaseConstants.USER_PHONE + ',' + databaseConstants.USER_RSA + ')'
                            ' VALUES'
                            ' ( \'' + phoneNumber + '\',\'' + rsaPubKey + '\') '
                            ' RETURNING ' + databaseConstants.USER_ID + ';')
        self.connection.commit()
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
    
    def delUser(self, userId):
        self.cursor.execute('DELETE FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
        self.connection.commit()
    
    def getUserPhone(self, userId):
        self.cursor.execute('SELECT ' + databaseConstants.USER_PHONE + ' FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
    
    def getUserName(self, userId):
        self.cursor.execute('SELECT ' + databaseConstants.USER_NAME + ' FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
    
    def getUserRSA(self, userId):
        self.cursor.execute('SELECT ' + databaseConstants.USER_RSA + ' FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
    
    def getUserId(self, userPhone):
        self.cursor.execute('SELECT ' + databaseConstants.USER_ID + ' FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_PHONE + ' = \'' + userPhone + '\' LIMIT 1 ;')
        try:
            return self.cursor.fetchone()[0]
        except (IndexError, TypeError):
            return None
    
    def updateUserRsa(self, userId, rsaPubKey):
        try:
            self.cursor.execute('UPDATE ' + databaseConstants.USER_TABLE + ' SET '
                                '' + databaseConstants.USER_RSA + ' = \'' + rsaPubKey + '\''
                                ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
            self.connection.commit()
        except (Exception, psycopg2.Error):
            self.connection.rollback()
            return False
        return True
        
    def updateUserName(self, userId, username):
        try:
            self.cursor.execute('UPDATE ' + databaseConstants.USER_TABLE + ' SET '
                                '' + databaseConstants.USER_NAME + ' = \'' + username + '\''
                                ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ' ;')
            self.connection.commit()
        except (Exception, psycopg2.Error):
            self.connection.rollback()
            return False
        return True
    
    def isUserExist(self, userId, userPhone):
        self.cursor.execute('SELECT EXISTS(SELECT 1 FROM ' + databaseConstants.USER_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.USER_PHONE + ' = \'' + userPhone + '\');')
        try:
            return self.cursor.fetchone()[0]
        except (IndexError, TypeError):
            return False
    
    # Query on the conversation table #
    
    def addConversation(self):
        self.cursor.execute('INSERT INTO ' + databaseConstants.CONVERSATION_TABLE + ''
                            ' (' + databaseConstants.CONVERSATION_ID + ')'
                            ' VALUES ( DEFAULT ) ' + ''
                            ' RETURNING ' + databaseConstants.CONVERSATION_ID + ';')
        self.connection.commit()
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
    
    def delConv(self, conversationId):
        self.cursor.execute('DELETE FROM ' + databaseConstants.CONVERSATION_TABLE + ''
                            ' WHERE ' + databaseConstants.CONVERSATION_ID + ' = ' + str(conversationId) + ' ;')
        self.connection.commit()
    
    # Query on the user conversation association table #
    
    def addUserToConversation(self, userId, conversationId):
        try:
            self.cursor.execute('INSERT INTO ' + databaseConstants.USER_CONVERSATION_TABLE + ''
                                ' (' + databaseConstants.USER_ID + ',' + databaseConstants.CONVERSATION_ID + ') VALUES'
                                ' ( \'' + str(userId) + '\',\'' + str(conversationId) + '\');')
            self.connection.commit()
        except (Exception, psycopg2.Error):
            self.connection.rollback()
            return False
        return True
    
    def getUserInConversation(self, conversationId):
        self.cursor.execute('SELECT ' + databaseConstants.USER_ID + ' FROM ' + databaseConstants.USER_CONVERSATION_TABLE + ''
                            ' WHERE ' + databaseConstants.CONVERSATION_ID + ' = ' + str(conversationId) + ';')
        result = self.cursor.fetchall()
        users = [0] * len(result)
        for i in range(0, len(result)):
            users[i] = result[i][0]
        return users
    
    def getConversationOfUser(self, userId):
        self.cursor.execute('SELECT ' + databaseConstants.CONVERSATION_ID + ' FROM ' + databaseConstants.USER_CONVERSATION_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ';')
        result = self.cursor.fetchall()
        conversations = [0] * len(result)
        for i in range(0, len(result)):
            conversations[i] = result[i][0]
        return conversations
    
    def delUserFromConv(self, userId, conversationId):
        self.cursor.execute('DELETE FROM ' + databaseConstants.USER_CONVERSATION_TABLE + ''
                            ' WHERE ' + databaseConstants.CONVERSATION_ID + ' = ' + str(conversationId) + ''
                            ' AND ' + databaseConstants.USER_ID + ' = ' + str(userId) + ';')
        self.connection.commit()
    
    # Query on the user message fragment table #
    
    def addFragment(self, userId, content, messageId, fragmentId, isFinal):
        return self.addFragmentTime(userId, content, messageId, fragmentId, isFinal, int(time.time()))
    
    def addFragmentTime(self, userId, content, messageId, fragmentId, isFinal, time):
        try:
            self.cursor.execute('INSERT INTO ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                                ' (' + databaseConstants.USER_ID + ',' + databaseConstants.FRAGMENT_CONTENT + ',' + databaseConstants.MESSAGE_ID + ','
                                '' + databaseConstants.FRAGMENT_ID + ',' + databaseConstants.FRAGMENT_IS_FINAL + ',' + databaseConstants.FRAGMENT_TIMESTAMP + ')'
                                ' VALUES'
                                ' ( \'' + str(userId) + '\',\'' + content + '\',\'' + str(messageId) + '\',\''
                                '' + str(fragmentId) + '\',\'' + ('TRUE' if isFinal else 'FALSE') + '\',\'' + str(time) + '\');')
            self.connection.commit()
        except (Exception, psycopg2.Error):
            self.connection.rollback()
            return False
        return True
    
    def delFragment(self, userId, messageId, fragmentId):
        self.cursor.execute('DELETE FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ''
                            ' AND ' + databaseConstants.FRAGMENT_ID + ' = ' + str(fragmentId) + ';')
        self.connection.commit()
        
    def delAllFragment(self, userId, messageId):
        self.cursor.execute('DELETE FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ';')
        self.connection.commit()
        
    def getFragment(self, userId, messageId):
        self.cursor.execute('SELECT ' + databaseConstants.FRAGMENT_ID + ' FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ';')
        result = self.cursor.fetchall()
        fragments = [0] * len(result)
        for i in range(0, len(result)):
            fragments[i] = result[i][0]
        return fragments
    
    def getFragmentContent(self, userId, messageId):
        self.cursor.execute('SELECT ' + databaseConstants.FRAGMENT_CONTENT + ' FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ''
                            ' ORDER BY ' + databaseConstants.FRAGMENT_ID + ' ASC ;')
        result = self.cursor.fetchall()
        fragmentContent = [0] * len(result)
        for i in range(0, len(result)):
            fragmentContent[i] = result[i][0]
        return fragmentContent
    
    def getFragmentSize(self, userId, messageId):
        self.cursor.execute('SELECT ' + databaseConstants.FRAGMENT_ID + ' FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.FRAGMENT_IS_FINAL + ' = TRUE '
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ';')
        try:
            return self.cursor.fetchone()[0] + 1
        except TypeError:
            return 0
        
    def getFragmentTime(self, userId, messageId, fragmentId):
        self.cursor.execute('SELECT ' + databaseConstants.FRAGMENT_TIMESTAMP + ' FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.USER_ID + ' = ' + str(userId) + ''
                            ' AND ' + databaseConstants.MESSAGE_ID + ' = ' + str(messageId) + ''
                            ' AND ' + databaseConstants.FRAGMENT_ID + ' = ' + str(fragmentId) + ''
                            ' ORDER BY ' + databaseConstants.FRAGMENT_TIMESTAMP + ' ASC LIMIT 1;')
        try:
            return self.cursor.fetchone()[0]
        except TypeError:
            return None
            
    def cleanFragment(self, timeout):
        self.cursor.execute('SELECT ' + databaseConstants.USER_ID + ", " + databaseConstants.MESSAGE_ID + ''
                            ' FROM ' + databaseConstants.MESSAGE_FRAGMENT_TABLE + ''
                            ' WHERE ' + databaseConstants.FRAGMENT_TIMESTAMP + ' < ' + str(int(time.time()) - timeout) + ';')
        result = self.cursor.fetchall()
        for i in range(0, len(result)):
            self.delAllFragment(result[i][0], result[i][1])
