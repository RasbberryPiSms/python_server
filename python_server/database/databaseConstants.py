# Constants to connect #
HOST = "127.0.0.1"
PORT = "5432"
DATABASE = "sms_project"
USER = "theo"
PASSWORD = "MeKUDKH6Ww93bi4y"

# Constants for the message fragment table #
MESSAGE_FRAGMENT_TABLE = "message_fragment"
MESSAGE_ID = "message_id"
FRAGMENT_ID = "fragment_id"
FRAGMENT_CONTENT = "content"
FRAGMENT_IS_FINAL = "is_final"
FRAGMENT_TIMESTAMP = "timestamp"

# Constants for the user table #
USER_TABLE = "client_user"
USER_ID = "user_id"
USER_PHONE = "phone_number"
USER_RSA = "rsa_pub"
USER_NAME = "username"

# Constants for the user conversation association table #
USER_CONVERSATION_TABLE = "user_conversation"

# Constants for the conversation table #
CONVERSATION_TABLE = "conversation"
CONVERSATION_ID = "conversation_id"
