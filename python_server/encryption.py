import rsa.key
import rsa.core
import base64

def cypher(msg, public_key):
    """
    :params msg: Unencyphered message
    :type msg: String
    :params public_key: Public key of the client
    :type public_key: String
    """
    der = base64.standard_b64decode(public_key)
    key_pub_dest = rsa.PublicKey.load_pkcs1(der, 'DER')
    return rsa.encrypt(msg, key_pub_dest)

def decypher(msg, private_key):
    """
    :params msg: Cyphered message
    :type msg: String
    :params private_key: Private key of the RaspPi
    :type private_key: String
    """
    B64PRIV_DER = private_key.encode()
    PRIVATE_DER = base64.standard_b64decode(B64PRIV_DER)
    priv_pi = rsa.PrivateKey.load_pkcs1(PRIVATE_DER, 'DER')
    msg_unbased = base64.standard_b64decode(msg)
    return rsa.decrypt(msg_unbased, priv_pi)
