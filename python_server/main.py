# from python_server.threads import ThreadSafeQueue, RWThread, Serial
# from python_server.processing import ProcessThread
import time
from timeit import default_timer as timer
import serial
import math
from multiprocessing import Process, Lock, Value
from python_server.database.DBHelper import DBHelper
from python_server.protocol.message import Message
from python_server.bot.botApi import switchApi
from pcsk1_x509.simple_converter import x509_to_pkcs1
from pathlib import Path
import sys

# Method to clean the key
def clean(key):
    key = key.replace("-----BEGIN PUBLIC KEY-----", "")
    key = key.replace("-----BEGIN RSA PRIVATE KEY-----", "")
    key = key.replace("-----END PUBLIC KEY-----", "")
    key = key.replace("-----END RSA PRIVATE KEY-----", "")
    key = key.replace("\n", "")
    return key


with open((Path(__file__).parent / '../key/publicKey.pub').resolve(), 'r') as file:
    rsaPublicKey = clean(file.read())
with open((Path(__file__).parent / '../key/privateKey.pem').resolve(), 'r') as file:
    rsaPrivateKey = clean(file.read())
    
phone = serial.Serial("/dev/ttyUSB1", 115200, rtscts=True, dsrdtr=True, timeout=5)
phone2 = serial.Serial("/dev/ttyUSB2", 115200, rtscts=True, dsrdtr=True, timeout=5)
mutexMessageProcess = Lock()
mutexSend = Lock()
mutexMessageId = Lock()
idMessage_next = 1

# Constantes
REQ_SEP_ARGS = "!"
REQ_SEP_DESCR = ":"
REQ_SEP_DATA = ","

API_CHAR = "/"

TIMEOUT_FRAGMENT = 60 * 15  # 15 min is the set timeout for now

SERVER_ID = 0
BUILD_VERSION = 1

INVITE_MESSAGE = "Join us : https://gitlab.com/RasbberryPiSms/messagetopi "
INVITE_MESSAGE_P2 = " Invited you"

# Request de clé publique de la PI
def init(phoneNumber, message):
    with DBHelper() as database:
        if(database.getUserId(phoneNumber) is not None):
            database.delUser(database.getUserId(phoneNumber))
        database.addUser(phoneNumber, x509_to_pkcs1(message.split(':')[1]), "UNDEF")
        sendCustom(phoneNumber, 'RSA:' + rsaPublicKey)
 
# Send an invite to the given phone number with the given username in the message
def sendInvite(phoneNumber, userName):
    sendCustom(phoneNumber, INVITE_MESSAGE + userName + INVITE_MESSAGE_P2)

# Send an error to the given phone number
def error(phoneNumber):
    sendCustom(phoneNumber, "Error")

# To send uncrypted custom message (error...)
def sendCustom(phoneNumber, content):
    if len(content) < 160:
        with mutexSend:
            phone2.write(b'AT+CMGS="' + phoneNumber.encode() + b'"\r')
            phone2.write(content.encode('utf_8', 'strict'))
            phone2.write(bytes([26]))
            start = timer()
            while(True):
                time.sleep(0.1)
                line = phone2.readline().decode("utf-8")
                if("CMGS:" in line or "ERROR" in line or (timer() - start) > 5):
                    break
                
# Send the given message to the given phone number, the frag is handled in the message class
# If the message is fragmented, increase the idMessageNext value by 1
def send(rsaPub, phoneNumber, content, idSender, idDest, idMessage_next=None):
    with mutexSend:
        if(idMessage_next is None):
            longMessage = Message(str(content), BUILD_VERSION, idSender, idDest, 0)
        else:
            longMessage = Message(str(content), BUILD_VERSION, idSender, idDest, int(idMessage_next.value))
        messageArray = longMessage.prepare(rsaPub)
        if len(messageArray) > 1:
            idMessage_next.value = (idMessage_next.value + 1) % int(math.pow(2, 63))
        for message in messageArray:
            phone2.write(b'AT+CMGS="' + phoneNumber.encode() + b'"\r')
            phone2.write(message.encode())
            phone2.write(bytes([26]))
            start = timer()
            while(True):
                time.sleep(0.1)
                line = phone2.readline().decode("utf-8")
                if("CMGS:" in line or "ERROR" in line or (timer() - start) > 5):
                    break

# Handle standard message
def standardMessage(currentMessage, phoneNumber, idMessage_next):
    with DBHelper() as database:
        for i in database.getUserInConversation(currentMessage.get_id_receiver()):
            if i != currentMessage.get_id_sender():
                (Process(target=send, args=(database.getUserRSA(i), database.getUserPhone(i), currentMessage.get_clear_msg(),
                                            currentMessage.get_id_sender(), currentMessage.get_id_receiver(), idMessage_next))).start()
        if API_CHAR == currentMessage.get_clear_msg()[:min(len(currentMessage.get_clear_msg()), 1)]:
            botMessage = switchApi(currentMessage.get_clear_msg()[1:].split()[:1][0], currentMessage.get_clear_msg().split()[1:])
            for i in database.getUserInConversation(currentMessage.get_id_receiver()):
                (Process(target=send, args=(database.getUserRSA(i), database.getUserPhone(i), botMessage,
                                            SERVER_ID, currentMessage.get_id_receiver(), idMessage_next))).start()

# Handle the init when we need to send his id to the user
def requestUserIdMessage(currentMessage, phoneNumber):
    with DBHelper() as database:
        newUserId = database.getUserId(phoneNumber)
        database.updateUserName(newUserId, currentMessage.get_clear_msg())
        (Process(target=send, args=(database.getUserRSA(newUserId), phoneNumber, ("SET_ID" + REQ_SEP_DESCR + str(newUserId) + REQ_SEP_ARGS + "NUMERO:" + phoneNumber),
                                    SERVER_ID, 0))).start()

# Handle the request message see the gitlab page with the templates
def requestMessage(currentMessage, phoneNumber):  # noqa: C901
    with DBHelper() as database:
        contentArray = currentMessage.get_clear_msg().split(REQ_SEP_ARGS)
        firstArg = contentArray[0].split(REQ_SEP_DESCR)[0]
        
        # Conversation creation
        if firstArg == "CREATE_CONV" and len(contentArray[0].split(REQ_SEP_DESCR)) == 2:
            idUserOther = database.getUserId(contentArray[0].split(REQ_SEP_DESCR)[1])
            if idUserOther is not None:
                newIdConv = database.addConversation()
                database.addUserToConversation(idUserOther, newIdConv)
                database.addUserToConversation(currentMessage.get_id_sender(), newIdConv)
                (Process(target=send, args=(database.getUserRSA(currentMessage.get_id_sender()), phoneNumber,
                                            ("ADD_USER_CONV" + REQ_SEP_DESCR + str(newIdConv) + REQ_SEP_ARGS + "ID_USER" + REQ_SEP_DESCR + str(idUserOther)),
                                            SERVER_ID, 0))).start()
                (Process(target=send, args=(database.getUserRSA(idUserOther), contentArray[0].split(REQ_SEP_DESCR)[1],
                                            ("ADD_USER_CONV" + REQ_SEP_DESCR + str(newIdConv) + REQ_SEP_ARGS + "ID_USER" + REQ_SEP_DESCR + str(currentMessage.get_id_sender())),
                                            SERVER_ID, 0))).start()
            else:
                sendInvite(contentArray[0].split(REQ_SEP_DESCR)[1], database.getUserName(currentMessage.get_id_sender()))
                
        # Adding a user to an existing conversation
        elif firstArg == "ADD_USER_CONV":
            idConv = contentArray[0].split(REQ_SEP_DESCR)[1]
            idUserOther = database.getUserId(contentArray[1].split(REQ_SEP_DESCR)[1])
            if idUserOther is not None:
                userInConv = database.getUserInConversation(idConv)
                database.addUserToConversation(idUserOther, idConv)
                content = "ADD_USER_CONV" + REQ_SEP_DESCR + str(idConv) + REQ_SEP_ARGS + "ID_USER" + REQ_SEP_DESCR
                for i in userInConv:
                    if i != idUserOther:
                        content += str(i) + REQ_SEP_DATA
                        (Process(target=send, args=(database.getUserRSA(i), database.getUserPhone(i),
                                                    ("ADD_USER_CONV" + REQ_SEP_DESCR + str(idConv) + REQ_SEP_ARGS + "ID_USER" + REQ_SEP_DESCR + str(idUserOther)),
                                                    SERVER_ID, 0))).start()
                (Process(target=send, args=(database.getUserRSA(idUserOther), contentArray[1].split(REQ_SEP_DESCR)[1],
                                            (content),
                                            SERVER_ID, 0))).start()
            else:
                sendInvite(contentArray[0].split(REQ_SEP_DESCR)[1], database.getUserName(currentMessage.get_id_sender()))
                
        # Remove user from a conversation
        elif firstArg == "REMOVE_USER_CONV":
            idConv = contentArray[0].split(REQ_SEP_DESCR)[1]
            if currentMessage.get_id_sender() in database.getUserInConversation(idConv):
                database.delUserFromConv(currentMessage.get_id_sender(), idConv)
                for i in database.getUserInConversation(idConv):
                    (Process(target=send, args=(database.getUserRSA(i), database.getUserPhone(i),
                                                ("REMOVE_USER_CONV" + REQ_SEP_DESCR + str(idConv) + REQ_SEP_ARGS + "ID_USER" + REQ_SEP_DESCR + str(currentMessage.get_id_sender())),
                                                SERVER_ID, 0))).start()
        
        # Return the user information
        elif firstArg == "REQUEST_INFO":
            if database.getUserPhone(contentArray[0].split(REQ_SEP_DESCR)[1]) is not None and database.getUserName(contentArray[0].split(REQ_SEP_DESCR)[1]) is not None:
                content = ("USER_INFO"
                           '' + REQ_SEP_DESCR + contentArray[0].split(REQ_SEP_DESCR)[1] + ''
                           '' + REQ_SEP_DATA + database.getUserPhone(contentArray[0].split(REQ_SEP_DESCR)[1]) + ''
                           '' + REQ_SEP_DATA + database.getUserName(contentArray[0].split(REQ_SEP_DESCR)[1]))
                (Process(target=send, args=(database.getUserRSA(currentMessage.get_id_sender()), phoneNumber, content,
                                            SERVER_ID, 0))).start()

# Verify if the message is a fragment or not
# If the fragment is full (all the other
# part in the db) gather fragment, and send it like a standard message
# Else just forward it
def testFragment(currMess, phoneNumber):
    currentMessage = Message()
    currentMessage.reconstitute(currMess, rsaPrivateKey)
    with DBHelper() as database:
        database.cleanFragment(TIMEOUT_FRAGMENT)
        if currentMessage.is_frag():  # TODO
            innerMessage = currentMessage.get_inner()[0]
            database.addFragment(currentMessage.get_id_sender(), currMess,
                                 currentMessage.get_id_message(), innerMessage.get_pos(), innerMessage.is_last())
            fragSize = database.getFragmentSize(currentMessage.get_id_sender(), currentMessage.get_id_message())
            if fragSize > 0:
                fragContent = database.getFragmentContent(currentMessage.get_id_sender(), currentMessage.get_id_message())
                if len(fragContent) == fragSize:
                    database.delAllFragment(currentMessage.get_id_sender(), currentMessage.get_id_message())
                    newMessage = Message()
                    for i in fragContent:
                        newMessage.reconstitute(i, rsaPrivateKey)
                    return newMessage
            return None
        else:
            return currentMessage
                
# Split the message and send it to the different job (initialisation / request / standard message)
def splitMessage(phoneNumber, currMess, idMessage_next):
    with DBHelper() as database:
        currentMessage = testFragment(currMess, phoneNumber)
        if currentMessage is not None:
            if currentMessage.get_id_sender() == 0 and currentMessage.get_id_receiver() == 0:
                print("Request IdUser")
                requestUserIdMessage(currentMessage, phoneNumber)
            elif database.isUserExist(currentMessage.get_id_sender(), phoneNumber):
                if currentMessage.get_id_receiver() == 0:
                    print("Request")
                    requestMessage(currentMessage, phoneNumber)
                else:
                    print("Standard message")
                    standardMessage(currentMessage, phoneNumber, idMessage_next)
            else:
                print("Droped")
        else:
            print("FragmentAdded")
        sys.stdout.flush()

# Split the response between the rsa request and the other message types (see the above method)
def respond(mess, num, idMessage_next):
    print('---------------New message---------------------' + "   " + time.asctime(time.localtime(time.time())))
    print("Incoming: Number : " + num + " Content : " + mess)
    try:
        if(mess[0:3] == 'RSA'):
            init(num, mess)
        else:
            splitMessage(num, mess, idMessage_next)
    except (Exception):
        error(num)

# Main function, read message as they arrive
# Create new process to handle the message
def main():
    # with DBHelper() as database:
    #     database.clearAll()
    #     pubUser = 'MEgCQQCfEnDhcb1o5IeOde8cbw1RMM1RpcgKkELVqHCKVEe3lSJSgWiww/2vFj2+hOeNa9dU8iiG713bOH9oOdXfeJlfAgMBAAE='
    #     database.addUser("+33684412932", pubUser, "userTest")
    #     database.addUser("+33684412931", pubUser, "userTest2")
    #     database.addUser("+33684412933", pubUser, "userTest3")
    #     database.addUser("+33684412934", pubUser, "userTest4")
    try:
        time.sleep(0.5)
        # phone.write(b'AT+CPIN=1234\r')
        # time.sleep(5)
        print('Attente de message ')
        phone.write(b'AT+CMGF=1\r')
        phone2.write(b'AT+CMGF=1\r')
        time.sleep(0.5)
        idMessage_next = Value('d', 1)
        while(True):
            line = phone.readline().decode("utf-8")
            if("CMTI" in line):
                phone.write(b'AT+CMGL="ALL"\r')
                messageList = []
                senderList = []
                while("OK" not in line):
                    line = phone.readline().decode("utf-8")
                    if("CMGL:" in line):
                        senderList.append(line.split(",")[2].strip('"'))
                        messageList.append(phone.readline().decode("utf-8").rstrip())
                phone.write(b'AT+CMGD=1,1\r')
                for i in range(0, len(senderList)):
                    (Process(target=respond, args=(messageList[i], senderList[i], idMessage_next))).start()
            time.sleep(0.5)
            
    finally:
        with mutexSend and mutexMessageProcess:
            phone.close()
            phone2.close()
