from python_server import __version__
from python_server.database.DBHelper import DBHelper
import time  # noqa: F401

def test_version():
    assert __version__ == '0.1.0'

def test_database():
    global time
    with DBHelper("postgres", "5432", "testDb", "runner", "passwordRunner") as mydb:
        mydb.clearAll()
        # Test on the user table #
        phoneNumber = "1234567890"
        rsaKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJDyyqWHigLeTvWmPvlcIvtdsrELiWCuXVcvheSTZzU8HObhuaCzFNOsRBvi5rd9uC9+uUITtLvV+MZ8JdLtzyECAwEAAQ=="
        userName = "usernameTest"
        userId = mydb.addUser(phoneNumber, rsaKey, userName)
        assert phoneNumber == mydb.getUserPhone(userId)
        assert mydb.getUserPhone(userId + 2) is None
        assert rsaKey == mydb.getUserRSA(userId)
        assert mydb.getUserRSA(userId + 2) is None
        assert userName == mydb.getUserName(userId)
        assert mydb.getUserName(userId + 2) is None
        assert 1 == mydb.getUserId(phoneNumber)
        assert mydb.getUserId(phoneNumber + "a") is None
        mydb.updateUserRsa(userId, rsaKey + "a")
        mydb.updateUserName(userId, userName + "a")
        assert userName + "a" == mydb.getUserName(userId)
        assert rsaKey + "a" == mydb.getUserRSA(userId)
        userId2 = mydb.addUserWithoutName(phoneNumber + 'a', rsaKey)
        assert mydb.isUserExist(userId2, phoneNumber) is False
        assert mydb.isUserExist(userId, phoneNumber) is True
        assert mydb.isUserExist(userId2, phoneNumber + 'a') is True
        assert mydb.isUserExist(userId, phoneNumber + 'a') is False
        assert mydb.isUserExist(userId + 5, phoneNumber) is False
        assert mydb.isUserExist(userId, phoneNumber + 'pkg') is False
        assert mydb.getUserName(userId2) is None
        mydb.updateUserName(userId2, userName)
        assert userName == mydb.getUserName(userId2)
        assert rsaKey == mydb.getUserRSA(userId2)
        assert mydb.getUserName(userId2 + 5) is None
        assert mydb.getUserRSA(userId2 + 5) is None
        
        # Test on the conversation table #
        convId = mydb.addConversation()
        
        # Test on the user conversation association table #
        assert mydb.addUserToConversation(userId, convId) is True
        assert mydb.addUserToConversation(userId + 2, convId) is False
        assert mydb.addUserToConversation(userId, convId + 2) is False
        assert 1 == mydb.getUserInConversation(convId)[0]
        assert 1 == mydb.getConversationOfUser(userId)[0]
        assert mydb.addUserToConversation(userId2, convId) is True
        assert [1, 2] == mydb.getUserInConversation(convId)
        assert [] == mydb.getUserInConversation(userId + 5)
        mydb.delUser(userId2)
        
        # Test on the message fragment table #
        messageId = 1
        fragmentId = 0
        assert mydb.addFragment(userId, "test01", messageId, fragmentId, False) is True
        assert mydb.addFragment(userId, "test01", messageId, fragmentId + 1, True) is True
        assert mydb.addFragment(userId + 2, "test01", messageId + 1, fragmentId, False) is False
        assert mydb.addFragment(userId, "test01", messageId, fragmentId, False) is False
        assert mydb.addFragment(userId, "test01", messageId, fragmentId + 1, True) is False
        assert [0, 1] == mydb.getFragment(userId, messageId)
        assert [0, 2] != mydb.getFragment(userId, messageId)
        assert [] == mydb.getFragment(userId, messageId + 1)
        mydb.delAllFragment(userId, messageId)
        time = int(time.time())
        timeout = 500
        assert mydb.addFragmentTime(userId, "test01", messageId, fragmentId, False, time) is True
        assert mydb.addFragmentTime(userId, "test02", messageId, fragmentId + 1, False, time - (timeout + 20)) is True
        assert mydb.addFragmentTime(userId, "test12", messageId + 1, fragmentId + 1, False, time) is True
        assert mydb.addFragmentTime(userId, "test11", messageId + 1, fragmentId, False, time) is True
        assert time == mydb.getFragmentTime(userId, messageId, fragmentId)
        assert time - (timeout + 20) == mydb.getFragmentTime(userId, messageId, fragmentId + 1)
        mydb.cleanFragment(timeout)
        assert mydb.getFragmentTime(userId, messageId, fragmentId) is None
        assert mydb.getFragmentTime(userId, messageId, fragmentId + 1) is None
        assert time == mydb.getFragmentTime(userId, messageId + 1, fragmentId)
        assert len(mydb.getFragmentContent(userId, messageId + 1)) != mydb.getFragmentSize(userId, messageId + 1)
        assert mydb.addFragmentTime(userId, "test13", messageId + 1, fragmentId + 2, True, time) is True
        assert len(mydb.getFragmentContent(userId, messageId + 1)) == mydb.getFragmentSize(userId, messageId + 1)
        assert ['test11', 'test12', 'test13'] == mydb.getFragmentContent(userId, messageId + 1)
        
        # Delete cascade test #
        mydb.delUser(userId)
        assert [] == mydb.getConversationOfUser(userId)
        assert mydb.getFragmentTime(userId, messageId + 1, fragmentId) is None
        mydb.clearAll()
