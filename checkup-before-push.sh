#!/bin/bash
DOCTEST_EXCLUDE_REGEX="(__init__.py$|\/__pycache__\/|main.py$)"

poetry run python3.7 -m flake8 python_server/ tests/
poetry run pytest
poetry run python3.7 -m doctest $(find python_server -type f | grep -Ev $DOCTEST_EXCLUDE_REGEX)
