# Python-Server 
![License](https://img.shields.io/badge/License-BSD%20%2F%20Apache%202-yellow)
[![pipeline status](https://gitlab.com/RasbberryPiSms/python_server/badges/master/pipeline.svg)](https://gitlab.com/RasbberryPiSms/python_server/-/commits/master)
[![Protocole](https://img.shields.io/badge/documentation-wiki-purple?style=flat-square)](https://gitlab.com/RasbberryPiSms/python_server/-/wikis/Documentation-protocole)
[![Documentation Jupyter](https://img.shields.io/badge/documentation-jupyter-purple?style=flat-square)](https://rasbberrypisms.gitlab.io/python_server)

Ce serveur python sert avec l'application [SmsToPi](https://gitlab.com/RasbberryPiSms/messagetopi), il permet d'avoir de meilleurs conversations de groupe, que celles de base permises par les sms et permet d'avoir des conversatiosn chiffrées entre client <=> serveur.

## Installation
Ces instructions vont vous permettrent d'installer ce serveur et de l'executer sur un serveur linux vierge (Dans le cadre de notre projet un RaspberryPi).

### Dépendances
- [python3](https://www.python.org/) - *Avec `pip` et `python3-venv`*
- [poetry](https://python-poetry.org/)
- [postgresql](https://www.postgresql.org/) - *Un serveur postgresql installé et configuré*
    - Toute la config Postgres est dans `{repo_root}/python_server/database/databaseConstants.py`
- Une clé branchée sur le Serial 1 capable d'envoyer des SMS *E.G. nous on utilise un MODEM 3G ZTE MF190 par exemple*

#### Exemple pour Debian
*A executer sur un serveur propre et fraichement initialisé*
```sh
#!/bin/bash
# Installation de dépendances
apt install git 
apt install postgresql postgresql-contrib postgresql-server-dev-all
apt install python3 python3-pip python3-venv python3-psycopg2
pip3 install poetry

# Clone de la repo et des modules
git clone https://gitlab.com/RasbberryPiSms/python_server.git --recurse-submodules

# Initialisation de poetry
cd python_server
poetry install

# Génération des clés publiques et privées
mkdir key
openssl genrsa -out key/privateKey.pem 512
openssl rsa -in key/privateKey.pem -pubout > key/publicKey.pub

# Configuration de postgres
su - postgres -c "createuser theo"
su - postgres -c "createdb sms_project"
su - postgres -c "psql -c 'grant all privileges on database sms_project to theo;'"
su - postgres -c "psql -c \"alter user theo with password 'MeKUDKH6Ww93bi4y'\""
poetry run main
```

## Service
```conf
[Unit]
Description=Python Server for the SmsToPi Project

[Service]
WorkingDirectory={repo_absolute_path}
ExecStart={user_home_dir}/.local/bin/poetry run main
StandardOutput=file:{user_home_dir}/.config/systemd/user/serverSms.log
StandardError=file:{user_home_dir}/.config/systemd/user/serverSms.log
```

## License
Ce projet est licensé sous double license [AGPL2 et BSD](https://gitlab.com/RasbberryPiSms/python_server/-/tree/master/licenses)

## Auteurs
- **Théo REY-VIVIANT**
- **Johan PLANCHON**
- **Johann HUGON**

## Sources
- ASN.1
    - https://coolaj86.com/articles/asn1-for-dummies/ *(Guide complet sur l'ASN.1)*
    - https://stackoverflow.com/questions/18039401/how-can-i-transform-between-the-two-styles-of-public-key-format-one-begin-rsa
- X.509 / PKCS
    - https://en.wikipedia.org/wiki/PKCS_8
    - https://en.wikipedia.org/wiki/X.509
- RSA
    - https://tls.mbed.org/kb/cryptography/rsa-encryption-maximum-data-size *(Taille max des données contenues dans le RSA)*
    - https://github.com/digitalbazaar/forge/issues/108 *(Une issue Github pour comprendre pourquoi)*
- AT commands 
    - https://stackoverflow.com/questions/2161197/how-to-send-receive-sms-using-at-commands
    - https://www.technologuepro.com/gsm/commande_at.htm
- Mutex https://stackoverflow.com/questions/3310049/proper-use-of-mutexes-in-python
- Powerpoint https://www.youtube.com/watch?v=LArkm4v5mWA
- Alternative https://hristoborisov.com/index.php/projects/turning-the-raspberry-pi-into-a-sms-center-using-python
- Utilisation Poetry https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-2/

