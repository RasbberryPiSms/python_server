from python_server.tools import Bitarray

class Header(object):
    """
    :Example:

    >>> from python_server.protocol import Header
    >>> el = Header(1, 128, 255, 'N', 0, 7)
    >>> h = el.get_header_as_string()
    >>> print(f"Header:{h} Length:{len(h)}")
    Header:00000000000000014e00000000000000000000000000000007008000ff Length:58
    >>> print(el)
    Header(
    Id build:0000000000000001:1
    Sender:0080:128
    Dest:00ff:255
    Frag_flag:N
    Frag_last:True
    Pos:0000000000000000:0
    Id:0000000000000007:7
    )
    """

    # 3o = 6hex
    header_len = 2 * 29
    begin = 0
    id_build = begin + 8 * 8
    flag_frag = id_build + 8 * 1
    id_frag = flag_frag + 8 * 8
    id_mess = id_frag + 8 * 8
    id_sender = id_mess + 8 * 2
    id_dest = id_sender + 8 * 2

    # Public
    def __init__(self, id_build, id_sender, id_dest, frag, pos, id_mess):
        super().__init__()
        self.header = Bitarray(Header.header_len * 4)

        self.set_id_build(id_build)
        self.set_sender(id_sender)
        self.set_dest(id_dest)
        self.set_frag(frag)
        self.set_pos(pos)
        self.set_id_mess(id_mess)

    def __str__(self):
        h = self.header
        build_ver = h.hex(Header.begin, Header.id_build)
        frag = h.hex(Header.id_build, Header.flag_frag)
        pos = h.hex(Header.flag_frag, Header.id_frag)
        id_m = h.hex(Header.id_frag, Header.id_mess)
        send = h.hex(Header.id_mess, Header.id_sender)
        dest = h.hex(Header.id_sender, Header.id_dest)
        return f"Header(\nId build:{build_ver}:{int(build_ver, 16)}\nSender:{send}:{int(send, 16)}\nDest:{dest}:{int(dest, 16)}\nFrag_flag:{bytes.fromhex(frag).decode('utf-8', 'strict')}\nFrag_last:{self.is_last()}\nPos:{pos}:{int(pos, 16)}\nId:{id_m}:{int(id_m, 16)}\n)"

    def set_id_build(self, ver):
        self.header.setbin_to_val(Header.begin, Header.id_build, ver)

    def set_frag(self, char):
        self.header.setbin_to_val(Header.id_build, Header.flag_frag, char)

    def is_frag(self):
        char = bytes.fromhex(self.header.hex(Header.id_build, Header.flag_frag)).decode('utf-8', 'strict')
        return char == 'F' or char == 'L'

    def is_last(self):
        char = bytes.fromhex(self.header.hex(Header.id_build, Header.flag_frag)).decode('utf-8', 'strict')
        return char == 'L' or char == 'N'

    def set_pos(self, pos):
        self.header.setbin_to_val(Header.flag_frag, Header.id_frag, pos)

    def get_pos(self):
        return int(self.header.hex(Header.flag_frag, Header.id_frag), 16)

    def set_id_mess(self, id_m):
        self.header.setbin_to_val(Header.id_frag, Header.id_mess, id_m)

    def get_id_mess(self):
        return int(self.header.hex(Header.id_frag, Header.id_mess), 16)

    def set_sender(self, id_s):
        self.header.setbin_to_val(Header.id_mess, Header.id_sender, id_s)

    def get_sender(self):
        return int(self.header.hex(Header.id_mess, Header.id_sender), 16)
    
    def set_dest(self, id_d):
        self.header.setbin_to_val(Header.id_sender, Header.id_dest, id_d)

    def get_dest(self):
        return int(self.header.hex(Header.id_sender, Header.id_dest), 16)

    def get_header_as_bytes(self):
        return bytes.fromhex(self.header.tohex())
    
    def get_header_as_bits(self):
        return self.header.to01()

    def get_header_as_string(self):
        return self.header.tohex()
