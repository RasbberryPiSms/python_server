from python_server.protocol import Header

class InnerMessage(object):
    # As hex 1hex = 0.5o
    # 60o = 120hex
    max_length = (49 * 2)

    def __init__(self, hex_msg, id_build, sender, dest, frag, pos, id_mess):
        """
        :params clear_msg: str(hexa)
        """
        super().__init__()
        if len(hex_msg) > self.max_length:
            raise ValueError
        else:
            self.msg = hex_msg
            self.header = Header(id_build, sender, dest, frag, pos, id_mess)
        
    def get_as_bits(self):
        msg_cmp = bin(int(self.msg, 16))[2:]
        msg = "0" * (len(self.msg) * 4 - len(msg_cmp))
        res = f"{self.header.get_header_as_bits()}{msg}{msg_cmp}"
        return res

    def get_as_hex(self):
        return f"{self.header.get_header_as_string()}{self.msg}"

    def get_id_mess(self):
        return self.header.get_id_mess()

    def get_pos(self):
        return self.header.get_pos()

    def is_last(self):
        return self.header.is_last()

    def is_frag(self):
        return self.header.is_frag()

    def get_send(self):
        return self.header.get_sender()

    def get_dest(self):
        return self.header.get_dest()

    def get_crypted_part(self):
        return self.get_as_hex()[25 * 2:]

    def get_uncrypted_part(self):
        return self.get_as_hex()[:25 * 2]

    def __str__(self):
        return f"InnerMessage(\nlen: {len(self.get_as_bits())}bits, inner: {self.msg},\nheader:{self.header})"
