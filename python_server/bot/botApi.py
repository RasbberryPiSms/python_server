import requests
import json

weatherApiKey = "5b305c7b3c9375938b96be3ae45b0e7c"
weatherApiurl = "http://api.openweathermap.org/data/2.5/weather?q="
weatherApiParam = "&units=metric&APPID="

catFactsApiurl = "https://cat-fact.herokuapp.com/facts/random"
catFactsApiParam = "?animal_type="


def switchApi(api, args=None):
    if api.lower() == "weather" and args is not None:
        if len(args) == 2:
            return (getWeather(args[0], args[1]))
    elif api.lower() == "facts":
        if args is not None and len(args) == 1:
            return (getCatFacts(args[0]))
        else:
            return (getCatFacts())


def getWeather(city, country):
    result = requests.get(weatherApiurl + city + ''
                          "," + country + weatherApiParam + weatherApiKey)
    data = json.loads(result.content)
    try:
        return ("The temperature in " + city + ''
                " is " + str(data.get('main').get('feels_like')) + "°c")
    except (Exception):
        return ("Something is wrong with your city or country")

def getCatFacts(animal=None):
    if animal is not None:
        result = requests.get(catFactsApiurl + catFactsApiParam + animal)
    else:
        result = requests.get(catFactsApiurl)
    try:
        data = json.loads(result.content)
        return (str(data.get('text')))
    except (Exception):
        return ("No facts on " + animal + ''
                ", that's too bad, try another one !")
