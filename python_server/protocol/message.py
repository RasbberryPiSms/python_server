import math
import base64

from python_server.protocol import Header
from python_server.protocol import InnerMessage
from python_server.encryption import cypher, decypher

class Message(object):
    # As Hex (0u16 = 0000u2 et 00hex = 0 as bytes)
    max_length = (2 ^ 64) * InnerMessage.max_length

    def __init__(self, clear_msg=-1, build_version=-1, sender=-1, dest=-1, id_mess=-1):
        super().__init__()
        if not (clear_msg == -1 or build_version == -1 or sender == -1 or dest == -1 or id_mess == -1):
            msg = clear_msg.encode(encoding="utf_8", errors="strict").hex()
            if len(msg) > self.max_length:
                raise ValueError

            self.inner = []
            nb_msg = math.ceil(len(msg) / InnerMessage.max_length)
            if len(msg) <= InnerMessage.max_length:
                self.inner.append(InnerMessage(msg, build_version, sender, dest, 'N', 0, id_mess))
            else:
                cnt = 0
                nb_inner = 1
                while nb_inner <= nb_msg:
                    is_last = nb_inner == nb_msg
                    if is_last:
                        flag = 'L'
                    else:
                        flag = 'F'
                    self.inner.append(InnerMessage(msg[cnt:(InnerMessage.max_length * nb_inner)], build_version, sender, dest, flag, nb_inner - 1, id_mess))
                    cnt += InnerMessage.max_length
                    nb_inner += 1
        else:
            self.inner = []

    def __str__(self):
        res = f"Message(\nFull len: {self.global_len()}bits\nInner:\n"

        for c in self.inner:
            res += f"{c}\n"
        
        res += ")"
        return res

    def get_id_sender(self):
        return self.inner[0].get_send()

    def get_id_receiver(self):
        return self.inner[0].get_dest()

    def get_id_message(self):
        return self.inner[0].get_id_mess()
    
    def get_as_bits(self):
        res = ""
        for c in self.inner:
            res += c.get_as_bits()

        return res

    def get_as_hex(self):
        res = ""
        for c in self.inner:
            res += c.get_as_hex()

        return res

    def global_len(self):
        length = 0
        for c in self.inner:
            length += len(c.get_as_bits())

        return length

    def is_frag(self):
        return self.inner[0].is_frag()

    def get_inner(self):
        return self.inner

    def reconstitute(self, el, privkey):  # el as bits_string
        mess_parts = el.split("==")
        header_uncrypt = base64.b64decode(f"{mess_parts[0]}==").hex()
        body_uncrypt = decypher(f"{mess_parts[1]}==", privkey).hex()
        baselen_fullm = f"{header_uncrypt}{body_uncrypt}"
        fullm = bin(int(baselen_fullm, 16))[2:]
        fullm = f"{'0' * (len(baselen_fullm) * 4 - len(fullm))}{fullm}"
        el = fullm

        if len(el) <= (InnerMessage.max_length + Header.header_len) * 4:
            build = int(el[Header.begin:Header.id_build], 2)
            frag = int(el[Header.id_build:Header.flag_frag], 2)
            pos = int(el[Header.flag_frag:Header.id_frag], 2)
            id_m = int(el[Header.id_frag:Header.id_mess], 2)
            send = int(el[Header.id_mess:Header.id_sender], 2)
            dest = int(el[Header.id_sender:Header.id_dest], 2)
            for e in self.inner:
                if e.get_id_mess() != id_m or e.get_pos() == pos:
                    # !! ERROR NOT SAME MESSAGE
                    break
            content = hex(int(el[Header.header_len * 4:], 2))[2:]
            inner = InnerMessage(content, build, send, dest, frag, pos, id_m)
            self.inner.append(inner)
            self.inner.sort(key=lambda x: x.get_pos())
            
            # Retourne True si le message est complet
            cnt = 0
            complete = False
            for e in self.inner:
                if e.get_pos() == cnt:
                    cnt += 1
                    if e.is_last():
                        complete = True
                else:
                    # !! Error while parsing
                    break
            return complete
        else:
            print("TooBig")
            exit(-1)

    def get_clear_msg(self):
        msg = ""
        complete = False
        cnt = 0
        for e in self.inner:
            if e.get_pos() == cnt:
                cnt += 1
                msg += e.msg
                if e.is_last():
                    complete = True
            else:
                # !! Error while parsing
                break

        if complete:
            msg = f"{'0' * (len(msg) % 2)}{msg}"
            return bytes.fromhex(msg).decode(encoding="utf_8", errors="strict")
        else:
            return None

    def prepare(self, pubkey):
        parts = []
        for frag in self.inner:
            header = frag.get_uncrypted_part()
            head_and_body = frag.get_crypted_part()
            clear_part = base64.b64encode(bytes.fromhex(header)).decode('utf_8', 'strict')
            crypted_part = bytes.fromhex(head_and_body)
            crypted_part = base64.b64encode(cypher(crypted_part, pubkey)).decode('utf_8', 'strict')
            msg = f"{clear_part}{crypted_part}"
            parts.append(msg)
        
        return parts
