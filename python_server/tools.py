from inspect import currentframe, getframeinfo

def current_filename():
    """
    :rtype: str
    """
    return getframeinfo(currentframe()).filename

def current_line_number():
    """
    :rtype: int
    """
    return getframeinfo(currentframe()).lineno

def debug_state():
    """
    Compile current_filename() and current_line_number()
    
    :rtype: str
    """
    return current_filename() + ':' + current_line_number() + ':'

class Bitarray(object):
    def __init__(self, length=0, basevalue=0):
        super().__init__()
        self.inner = [basevalue] * length

    def __len__(self):
        return len(self.inner)

    def __setitem__(self, k, v):
        if type(v) == bool:
            if v: self.inner[k] = 1
            else: self.inner[k] = 0
        elif type(v) == list:
            val = [0] * len(v)
            for e in range(0, len(v)):
                if v[e]:
                    val[e] = 1
                else: val[e] = 0
            self.inner[k] = val

    def __getitem__(self, k):
        return self.inner[k]

    def __delitem__(self, k):
        self.inner[k] = None
    
    def __iter__(self):
        for e in range(0, len(self.inner)):
            yield self.inner[e]
    
    def from_str(string=""):
        res = Bitarray()
        res.inner = [0] * len(string)
        for c in range(0, len(string)):
            res.__setitem__(c, string[c] == "1")
        
        return res

    def setbin_to_val(self, from_index, to_index, integ_or_char):
        """
        Permet de mettre la valeur en binaire de integ_or_char dans un tableu de bit
        :type bitarr: bitarray()
        :type integ: int or char
        :rtype: bitarray()
        """
        if type(integ_or_char) == int:
            raw = bin(integ_or_char)[2:]
        else:
            raw = bin(int(integ_or_char.encode(encoding="utf_8", errors="strict").hex(), 16))[2:]

        if len(self[from_index:to_index]) < len(raw):
            raise ValueError
        else:
            for c in range(0, len(raw)):
                if '0' == raw[len(raw) - 1 - c]: el = False
                else: el = True
                self[to_index - 1 - c] = el

    def hex(self, from_index, to_index):
        len_correct = self.to01()[from_index:to_index]
        hexa = hex(int(len_correct, 2))[2:]
        return f"{'0' * (len(len_correct) // 4 - len(hexa))}{hexa}"

    def to01(self):
        return "".join(str(e) for e in self.inner)

    def tohex(self):
        if len(self.inner) % 4 == 0:
            hexa = hex(int(self.to01(), 2))[2:]
            return f"{'0' * (len(self.inner) // 4 - len(hexa))}{hexa}"
