c = get_config()
c.NbConvertApp.notebooks = ["*.ipynb"]
c.NbConvertApp.export_format = "pdf"
c.FilesWriter.build_directory = "public"
